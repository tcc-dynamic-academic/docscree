package br.ueg.unucet.docscree.controladores;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import br.ueg.unucet.docscree.utilitarios.BloquearArtefatoControle;
import br.ueg.unucet.docscree.utilitarios.enumerador.TipoMensagem;
import br.ueg.unucet.docscree.visao.compositor.ArtefatoPreenchidoCompositor;
import br.ueg.unucet.docscree.visao.compositor.SuperArtefatoCompositor;
import br.ueg.unucet.quid.dominios.Artefato;
import br.ueg.unucet.quid.dominios.ArtefatoPreenchido;
import br.ueg.unucet.quid.dominios.ArtefatoServico;
import br.ueg.unucet.quid.dominios.ItemModelo;
import br.ueg.unucet.quid.dominios.MembroDocScree;
import br.ueg.unucet.quid.dominios.Projeto;
import br.ueg.unucet.quid.dominios.Retorno;
import br.ueg.unucet.quid.dominios.Usuario;
import br.ueg.unucet.quid.excessoes.QuidExcessao;
import br.ueg.unucet.quid.extensao.dominios.Membro;
import br.ueg.unucet.quid.extensao.implementacoes.Parametro;
import br.ueg.unucet.quid.extensao.implementacoes.ServicoPersistencia;
import br.ueg.unucet.quid.extensao.implementacoes.SuperTipoMembroVisaoZK;
import br.ueg.unucet.quid.extensao.interfaces.IParametro;
import br.ueg.unucet.quid.extensao.interfaces.IServico;
import br.ueg.unucet.quid.extensao.interfaces.ITipoMembroVisao;
import br.ueg.unucet.quid.extensao.utilitarias.FabricaParametros;
import br.ueg.unucet.quid.gerenciadorservico.ioc.ContextoServicos;
import br.ueg.unucet.quid.servicosquid.ServicoValidacao;
import br.ueg.unucet.quid.utilitarias.FabricaSerializacao;
//import br.ueg.unucet.plugin.service.persistenciabancodados1.service.ServicoPersistenciaBD;
import br.ueg.unucet.quidgerenciadorservico.QuidGerenciadorServico;

/**
 * Controlador específico do caso de uso Montar ArtefatoModelo
 * 
 * @author Diego
 *
 */
@SuppressWarnings("rawtypes")
public class ArtefatoControle extends SuperArtefatoControle {

	// /TODO JAVADOC
	// @Autowired
	// TODO COLOCAR NO SPRING
	private ServicoControle servicoControle = new ServicoControle();

	private ContextoServicos contextoServicos = new ContextoServicos(
			new URL[] {});

	/**
	 * @see br.ueg.unucet.docscree.interfaces.ICRUDControle#acaoSalvar()
	 */
	@Override
	public boolean acaoSalvar() {
		if (this.isUsuarioMontador()) {
			Retorno<String, Collection<String>> retorno;
			Artefato instanciaArtefato = super.getFramework()
					.getInstanciaArtefato();
			instanciaArtefato.setNome(super.getEntidade().getNome());
			instanciaArtefato.setDescricao(super.getEntidade().getDescricao());
			instanciaArtefato.setAltura(super.getEntidade().getAltura());
			instanciaArtefato.setLargura(super.getEntidade().getLargura());
			retorno = super.getFramework().mapearArtefato(instanciaArtefato);
			if (retorno.isSucesso()) {
				Artefato artefatoLancao = lancarArtefatoNaVisao((SuperArtefatoCompositor) getVisao());
				BloquearArtefatoControle.obterInstancia()
						.adicionarBloqueioArtefato(artefatoLancao,
								getUsuarioLogado());
			}
			return super.montarRetorno(retorno);
		}
		return false;
	}

	/**
	 * Método que abre um ArtefatoModelo, joga a entidade para a visão para
	 * montagem
	 * 
	 * @return boolean se ação foi executada ou não
	 */
	public boolean acaoAbrirArtefato() {
		setarEntidadeVisao(getVisao());
		boolean membrosMapeados = ((SuperArtefatoCompositor) getVisao())
				.mapearMembrosAoArtefato();
		boolean servicosMapeados = ((SuperArtefatoCompositor) getVisao())
				.mapearServicosAoArtefato();
		if (membrosMapeados) {
			return true;
		} else {
			getMensagens().setTipoMensagem(TipoMensagem.ERRO);
			getMensagens()
					.getListaMensagens()
					.add("O ArtefatoModelo contém TipoMembros ou Serviços que não pertencem ao DocScree, impossível abrí-lo!");
			return false;
		}
	}

	/**
	 * Método que abre um ArtefatoModelo, joga a entidade para a visão para
	 * montagem
	 * 
	 * @return boolean se ação foi executada ou não
	 */
	public boolean acaoAbrirArtefatoPreenchido() {
		setarEntidadeVisao(getVisao());
		boolean membrosMapeados = ((SuperArtefatoCompositor) getVisao())
				.mapearMembrosAoArtefato();
		boolean servicosMapeados = ((SuperArtefatoCompositor) getVisao())
				.mapearServicosAoArtefato();
		if (membrosMapeados) {
			return true;
		} else {
			getMensagens().setTipoMensagem(TipoMensagem.ERRO);
			getMensagens()
					.getListaMensagens()
					.add("O ArtefatoModelo contém TipoMembros ou Serviços que não pertencem ao DocScree, impossível abrí-lo!");
			return false;
		}
	}

	/**
	 * @see br.ueg.unucet.docscree.controladores.GenericoControle#executarListagem()
	 */
	@Override
	protected Retorno<String, Collection<Artefato>> executarListagem() {
		return getFramework().pesquisarArtefato(null, null, null);
	}

	/**
	 * Método que mapea um Membro, ou seja, faz a persistência de um novo Membro
	 * no framework, ainda não o relacionamento com o ArtefatoModelo
	 * 
	 * @return boolean se a ação foi executada
	 */
	public boolean acaoMapearMembro() {
		if (this.acaoRenovarBloqueio()) {
			Retorno<Object, Object> retorno = null;
			SuperTipoMembroVisaoZK<?> tipoMembroVisao = getTipoMembroVisao();
			retorno = getFramework().mapearMembro(tipoMembroVisao.getMembro());
			if (retorno.isSucesso()) {
				Retorno<String, Collection<Membro>> retornoPesquisa = getFramework()
						.pesquisarMembro(
								tipoMembroVisao.getMembro().getNome(),
								tipoMembroVisao.getMembro()
										.getTipoMembroModelo());
				Collection<Membro> collection = retornoPesquisa.getParametros()
						.get(Retorno.PARAMERTO_LISTA);
				Membro membroMapeado = collection.iterator().next();
				tipoMembroVisao.getMembro()
						.setCodigo(membroMapeado.getCodigo());
				return true;
			} else {
				getMensagens().getListaMensagens().add(retorno.getMensagem());
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Método que mapea um Serviço, ou seja, faz a persistência de um novo
	 * Serviço no ArtefatoModelo
	 * 
	 * @return boolean se a ação foi executada
	 */
	public boolean acaoMapearServico() {
		Retorno<Object, Object> retorno = null;
		if (this.acaoRenovarBloqueio()) {
			retorno = QuidGerenciadorServico.getInstancia()
					.getArtefatoServicoControle()
					.mapearServicoArtefato(getArtefatoServicoAMapear());
			if (retorno.isSucesso()) {
				@SuppressWarnings("unchecked")
				Retorno<String, Collection<ArtefatoServico>> retornoPesquisa = (Retorno<String, Collection<ArtefatoServico>>) QuidGerenciadorServico
						.getInstancia()
						.getArtefatoServicoControle()
						.pesquisarArtefatosServicoPorMomento(
								getArtefatoServicoAMapear().getArtefato(),
								getArtefatoServicoAMapear().getServico(),
								getArtefatoServicoAMapear().getMomentoDisparo());
				Collection<ArtefatoServico> collection = retornoPesquisa
						.getParametros().get(Retorno.PARAMERTO_LISTA);
				ArtefatoServico artefatoServicoMapeado = collection.iterator()
						.next();
				getArtefatoServicoAMapear().setCodigo(
						artefatoServicoMapeado.getCodigo());
				return true;
			} else {
				getMensagens().getListaMensagens().add(retorno.getMensagem());
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Método que altera um Serviço já cadastrado no framework para um
	 * determinado artefato Serviço no ArtefatoModelo
	 * 
	 * @return boolean se a ação foi executada
	 */
	public boolean acaoAlterarServico() {
		Retorno<Object, Object> retorno = null;
		if (this.acaoRenovarBloqueio()) {
			retorno = QuidGerenciadorServico.getInstancia()
					.getArtefatoServicoControle()
					.alterarServicoArtefato(getArtefatoServicoAMapear());
			return montarRetornoObject(retorno);
		} else {
			return false;
		}
	}

	/**
	 * Método que altera o Membro já persistido no framework
	 * 
	 * @return boolean se ação foi executada
	 */
	public boolean acaoAlterarMembro() {
		if (this.acaoRenovarBloqueio()) {
			Retorno<Object, Object> retorno = null;
			retorno = getFramework().alterarMembro(
					getTipoMembroVisao().getMembro());
			return montarRetornoObject(retorno);
		} else {
			return false;
		}
	}

	/**
	 * Ação que remove membro do framework
	 * 
	 * @return boolean se ação foi executada
	 */
	public boolean acaoRemoverMembro() {
		if (this.acaoRenovarBloqueio()) {
			Retorno<Object, Object> retorno = getFramework().removerMembro(
					getTipoMembroVisao().getMembro());
			return montarRetornoObject(retorno);
		} else {
			return false;
		}
	}

	/**
	 * Método que monta a mensagem de retorno através de um Retorno<Object,
	 * Object> vindo do framework
	 * 
	 * @param retorno
	 *            vindo do framework
	 * @return boolean resultado do Retorno
	 */
	private boolean montarRetornoObject(Retorno<Object, Object> retorno) {
		if (retorno.isSucesso()) {
			return true;
		} else {
			getMensagens().getListaMensagens().add(retorno.getMensagem());
			return false;
		}
	}

	public Artefato obterArtefatoModelo(ArtefatoPreenchido artefatoPreenchido) {
		Retorno<String, Artefato> retorno = getFramework().obterArtefatoModelo(
				artefatoPreenchido);
		if (retorno.isSucesso()) {
			return retorno.getParametros()
					.get(Retorno.PARAMETRO_NOVA_INSTANCIA);
		}
		return null;
	}

	public boolean acaoChamarServicoPersistencia() {
		ArtefatoPreenchido artefatoPreenchido = persistirArtefatoPreenchido();
		Collection<IParametro<?>> parametros = parametrizarServicoPersistencia(false);
		parametros.add(FabricaParametros.gerarParametroValor(
				Integer.class,
				ServicoPersistencia.PARAMETRO_VERSAO,
				artefatoPreenchido.getVersao()));
		parametros.add(FabricaParametros.gerarParametroValor(String.class,
				ServicoPersistencia.PARAMETRO_MODO,
				ServicoPersistencia.MODO_SALVAR));
		parametros.remove(FabricaParametros.gerarParametroValor(String.class,
				ServicoPersistencia.PARAMETRO_MODO,
				ServicoPersistencia.MODO_LEITURA));
		if (getMapaAtributos().get(
						"artefatoPreenchidoAberto") != null){
		parametros.remove(FabricaParametros.gerarParametroValor(
				Long.class,
				ServicoPersistencia.PARAMETRO_ID_ARTEFATO_PREENCHIDO,
				((ArtefatoPreenchido) getMapaAtributos().get(
						"artefatoPreenchidoAberto")).getCodigo()));
		}
		parametros.add(FabricaParametros.gerarParametroValor(
				Long.class,
				ServicoPersistencia.PARAMETRO_ID_ARTEFATO_PREENCHIDO,
				artefatoPreenchido.getCodigo()));
		Retorno<Object, Object> retorno = servicoControle.executaServico(
				getServicoPersistencia().getServico(), parametros,
				contextoServicos);
		if (retorno.isSucesso()) {
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	private ArtefatoPreenchido persistirArtefatoPreenchido(){
		ArtefatoPreenchido artefatoPreenchido = (ArtefatoPreenchido) getMapaAtributos()
				.get("artefatoPreenchidoAberto");
		if(artefatoPreenchido == null)
		{
			artefatoPreenchido = new ArtefatoPreenchido();
			artefatoPreenchido.setArtefato(getEntidade().getCodigo());
			artefatoPreenchido.setNome(getEntidade().getNome());
			artefatoPreenchido.setDescricao(getEntidade().getDescricao());
			artefatoPreenchido.setModelo(getProjeto().getModelo().getCodigo());
			artefatoPreenchido.setRevisao(1);
			artefatoPreenchido.setVersao(1);
			artefatoPreenchido.setUsuario(getUsuarioLogado().getCodigo());
			try {
				QuidGerenciadorServico.getInstancia().getArtefatoPreenchidoControle().inserir(artefatoPreenchido);
			} catch (QuidExcessao e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return artefatoPreenchido;
		}
		else
		{
			if (((Boolean) getMapaAtributos()
					.get("novaVersao")) == true){
				artefatoPreenchido.setVersao(artefatoPreenchido.getVersao() + 1);
				artefatoPreenchido.setCodigo(null);
				try {
					QuidGerenciadorServico.getInstancia().getArtefatoPreenchidoControle().inserir(artefatoPreenchido);
				} catch (QuidExcessao e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return artefatoPreenchido;
		
	}

	public boolean acaoChamarServicoPersistenciaLeitura() {
		ArtefatoPreenchido artefatoPreenchido = (ArtefatoPreenchido) getMapaAtributos()
				.get("artefatoPreenchidoAberto");
		if (artefatoPreenchido != null && artefatoPreenchido.getCodigo() > 0) {
			Collection<IParametro<?>> parametros = parametrizarServicoPersistencia(true);
			parametros.add(FabricaParametros.gerarParametroValor(
					Long.class,
					ServicoPersistencia.PARAMETRO_ID_ARTEFATO_PREENCHIDO,
					artefatoPreenchido.getCodigo()));
			parametros.add(FabricaParametros.gerarParametroValor(String.class,
					ServicoPersistencia.PARAMETRO_MODO,
					ServicoPersistencia.MODO_LEITURA));
			Retorno<Object, Object> retorno = servicoControle.executaServico(
					getServicoPersistencia().getServico(), parametros,
					contextoServicos);
			if (retorno.isSucesso()) {
				List<?> lista = (List<?>) retorno.getParametros().get(
						Retorno.PARAMETRO_LISTA_PARAMETRO_SERVICO);

				// Map<String, MembroDocScree> listaMembros = (Map<String,
				// MembroDocScree>)
				// (getParametroPorNome(ServicoPersistencia.PARAMETRO_LISTA_MEMBROS,
				// (Collection<IParametro>) lista)).getValor();
				if (getParametroPorNome(
						ServicoPersistencia.PARAMETRO_VALORES_MEMBROS,
						(Collection<IParametro>) lista) != null) {
					Map<String, Object> listaValores = (Map<String, Object>) FabricaSerializacao
							.obterInstancia()
							.obterObjeto(
									(byte[]) (getParametroPorNome(
											ServicoPersistencia.PARAMETRO_VALORES_MEMBROS,
											(Collection<IParametro>) lista))
											.getValor());
					if (listaValores != null) {
						for (String chave : listaValores.keySet()) {
							((Map<String, MembroDocScree>) getMapaAtributos()
									.get("listaMembrosDocScree")).get(chave);
							MembroDocScree membro = ((Map<String, MembroDocScree>) getMapaAtributos()
									.get("listaMembrosDocScree")).get(chave);
							((ArtefatoPreenchidoCompositor) getVisao())
									.lancarMembro(membro.getTipoMembroVisao(),
											listaValores.get(chave), chave);
						}
					}
				}
				return true;
			}
			return false;
		}
		return true;
	}

	private Collection<IParametro<?>> parametrizarServicoPersistencia(boolean leitura) {
		// Collection<IParametro<?>> parametros = new
		// ArrayList<IParametro<?>>();
		Collection<IParametro<?>> parametrosPer = (Collection<IParametro<?>>) FabricaSerializacao
				.obterInstancia().obterObjeto(
						getServicoPersistencia().getParametrosServico());
		Collection<IParametro<?>> parametrosPersistencia = new ArrayList<IParametro<?>>();
		parametrosPersistencia.addAll(parametrosPer);
		
		parametrosPersistencia.add(FabricaParametros
				.gerarParametroValor(
						Boolean.class,
						ServicoPersistencia.PARAMETRO_NOVA_VERSAO,
						(Boolean) getMapaAtributos()
								.get("novaVersao") ) );
		if (!leitura){
			parametrosPersistencia.add(FabricaParametros.gerarParametroValor(
					byte[].class,
					ServicoPersistencia.PARAMETRO_LISTA_MEMBROS,
					FabricaSerializacao.obterInstancia().serializarObjeto(
							getMapaAtributos().get("listaMembrosDocScree"))));
		
		
			parametrosPersistencia.add(FabricaParametros.gerarParametroValor(
					byte[].class,
					ServicoPersistencia.PARAMETRO_VALORES_MEMBROS,
					FabricaSerializacao.obterInstancia().serializarObjeto(
							getMapaAtributos().get("mapaValores"))));
		}
		parametrosPersistencia.add(FabricaParametros.gerarParametroValor(
				Long.class, ServicoPersistencia.PARAMETRO_PROJETO, getProjeto()
						.getCodigo()));
		/*
		 * getServicoPersistencia().setParametrosServico(
		 * FabricaSerializacao.obterInstancia().serializarObjeto(
		 * parametrosPersistencia));
		 */
		return parametrosPersistencia;

	}

	@SuppressWarnings("unchecked")
	public boolean acaoChamarServicoValidacao() {
		getEntidade().setMembros(new ArrayList<Membro>());
		Collection<IParametro<?>> parametros = new ArrayList<IParametro<?>>();
		criarParametrosSuperServico(parametros);
		parametros.add(criarParametroMapeador(
				(Map) getMapaAtributos().get("listaMembrosDocScree"),
				ServicoValidacao.PARAMETRO_MEMBROS));
		parametros.add(criarParametroMapeador(
				(Map) getMapaAtributos().get("mapaValores"),
				ServicoValidacao.PARAMETRO_VALORES_MEMBROS));
		Retorno<Object, Object> retorno = getEntidade().executaServico(
				"ServicoValidacaoSimples", parametros);
		Collection<Membro> listaMembros = null;
		Collection<String> listaMensagens = null;
		if (retorno.isSucesso()) {
			Collection<IParametro> parametrosServico = (Collection<IParametro>) retorno
					.getParametros().get(
							Retorno.PARAMETRO_LISTA_PARAMETRO_SERVICO);
			for (IParametro iParametro : parametrosServico) {
				if (iParametro.getNome().equals(
						ServicoValidacao.PARAMETRO_MEMBROS_RESULTADO)) {
					listaMembros = (Collection<Membro>) iParametro.getValor();
				} else if (iParametro.getNome().equals(
						ServicoValidacao.PARAMETRO_LISTA_ERROS)) {
					listaMensagens = (Collection<String>) iParametro.getValor();
				}
			}
			if (listaMensagens != null && !listaMensagens.isEmpty()) {
				getMensagens().getListaMensagens().addAll(listaMensagens);
				return false;
			}
			if (listaMembros != null) {
				((SuperArtefatoCompositor) getVisao()).setMembros(listaMembros);
			}
			return true;
		}
		return false;
	}

	private void criarParametrosSuperServico(
			Collection<IParametro<?>> parametros) {
		parametros.add(criarParametroArtefatoModelo());
		parametros.add(criarParametroProjeto());
		parametros.add(criarParametroUsuario());
	}

	protected IParametro<Artefato> criarParametroArtefatoModelo() {
		Parametro<Artefato> parametro = new Parametro<Artefato>(Artefato.class);
		parametro.setNome(IServico.PARAMETRO_ARTEFATO_MODELO);
		parametro.setValorClass(getEntidade());
		return parametro;
	}

	protected IParametro<Usuario> criarParametroUsuario() {
		Parametro<Usuario> parametro = new Parametro<Usuario>(Usuario.class);
		parametro.setNome(IServico.PARAMETRO_USUARIO);
		parametro.setValorClass(getUsuarioLogado());
		return parametro;
	}

	protected IParametro<Projeto> criarParametroProjeto() {
		Parametro<Projeto> parametro = new Parametro<Projeto>(Projeto.class);
		parametro.setNome(IServico.PARAMETRO_PROJETO);
		parametro.setValorClass(getProjeto());
		return parametro;
	}

	// /TODO outra forma de criar parametros
	public static IParametro<String> criarParametroString(String nome,
			String valor) {
		Parametro<String> parametro = new Parametro<String>(String.class);
		parametro.setNome(nome);
		parametro.setValorClass(valor);
		return parametro;
	}

	protected IParametro<Map> criarParametroMapeador(Map valor, String nome) {
		Parametro<Map> parametro = new Parametro<>(Map.class);
		parametro.setNome(nome);
		parametro.setValorClass(valor);
		return parametro;
	}

	protected IParametro<Integer> criarParametroInteiro(String nome,
			Integer valor) {
		Parametro<Integer> parametro = new Parametro<Integer>(Integer.class);
		parametro.setNome(nome);
		parametro.setValorClass(valor);
		return parametro;
	}

	protected IParametro<Long> criarParametroLong(String nome, Long valor) {
		Parametro<Long> parametro = new Parametro<Long>(Long.class);
		parametro.setNome(nome);
		parametro.setValorClass(valor);
		return parametro;
	}

	/**
	 * Retorna o SuperTipoMembroVisaoZK que foi selecionado na visão através do
	 * mapa de atributos
	 * 
	 * @return SuperTipoMembroVisaoZK tipoMembroVisaoSelecionado
	 */
	private SuperTipoMembroVisaoZK<?> getTipoMembroVisao() {
		return (SuperTipoMembroVisaoZK<?>) getMapaAtributos().get(
				"tipoMembroVisaoSelecionado");
	}

	/**
	 * Método que verifica se é possivel executar a montagem do ArtefatoModelo
	 * 
	 * @return boolean se o usuário tem papel de Montador
	 */
	public boolean acaoMontarArtefato() {
		return super.isUsuarioMontador();
	}

	/**
	 * Método que verifica se é o usuário é preenchedor
	 * 
	 * @return boolean se o usuário tem papel de Preenchedor
	 */
	public boolean acaoPreencherArtefato() {
		return super.isUsuarioPreenchedor();
	}

	/**
	 * Método que mapeia ArtefatoModelo, ou seja, altera o ArtefatoModelo e gera
	 * relacionamentos com os Serviços e Membros adicionados a ele.
	 * 
	 * @return boolean se ação foi executada
	 */
	public boolean acaoMapearArtefato() {
		boolean resultado = true;
		if (getEntidade().getCategoria() != null) {
			if (getEntidade().getCategoria().getCodigo() == null) {
				getEntidade().setCategoria(null);
			}
		}
		getFramework().alterarArtefato(getEntidade());
		return resultado;
	}

	/**
	 * Método que retorna um SuperTipoMembroVisaoZK a partir de um Membro
	 * 
	 * @param membro
	 * @return SuperTipoMembroVisaoZK do Membro
	 */
	public SuperTipoMembroVisaoZK<?> getTipoMembroVisaoPeloMembro(Membro membro) {
		Retorno<String, ITipoMembroVisao> retorno = getFramework()
				.getTipoMembroVisao(membro.getTipoMembroModelo());
		if (retorno.isSucesso()) {
			ITipoMembroVisao tipoMembroVisao = retorno.getParametros().get(
					Retorno.PARAMETRO_NOVA_INSTANCIA);
			if (tipoMembroVisao instanceof SuperTipoMembroVisaoZK) {
				return (SuperTipoMembroVisaoZK) tipoMembroVisao;
			}
		}
		return null;
	}

	/**
	 * Método que retorna a Coleção de TipoMembroVisao do Framework, sem
	 * converter em SuperTipoMembroVisaoZK ou analisar se é um TipoMembroVisao
	 * do DocScree
	 * 
	 * @return Coleção de ITipoMembroVisao
	 */
	public Collection<ITipoMembroVisao> getMapaTipoMembrosVisao() {
		Retorno<String, Collection<ITipoMembroVisao>> retorno = super
				.getFramework().listaTipoMembroVisao();
		Collection<ITipoMembroVisao> lista = new ArrayList<ITipoMembroVisao>();
		if (retorno.isSucesso()) {
			lista = (Collection<ITipoMembroVisao>) retorno.getParametros().get(
					Retorno.PARAMERTO_LISTA);
		}
		return lista;
	}

	/**
	 * Método que retorna os TipoMembroVisao pertecentes ao DocScree cadastrados
	 * no Framework.
	 * 
	 * @return List<SuperTipoMembroVisaoZK>
	 */
	public List<SuperTipoMembroVisaoZK> listarTipoMembrosVisao() {
		List<SuperTipoMembroVisaoZK> lista = new ArrayList<SuperTipoMembroVisaoZK>();
		for (ITipoMembroVisao tipoMembroVisao : getMapaTipoMembrosVisao()) {
			if (tipoMembroVisao instanceof SuperTipoMembroVisaoZK) {
				lista.add((SuperTipoMembroVisaoZK) tipoMembroVisao);
			}
		}
		return lista;
	}

	/**
	 * Método que lista os ArtefatosModelos cadastrados no Framework para
	 * abri-lo e editá-lo.
	 * 
	 * @return List<Artefato>
	 */
	public List<Artefato> listarArtefatosModelo() {
		Retorno<String, Collection<Artefato>> retorno = executarListagem();
		if (retorno.isSucesso()) {
			// TODO tratar para listar somentes ArtefatosModelo não preenchido.
			Collection<Artefato> collection = retorno.getParametros().get(
					Retorno.PARAMERTO_LISTA);
			return new ArrayList<Artefato>(collection);
		} else {
			return new ArrayList<Artefato>();
		}
	}

	/**
	 * Método que lista os ArtefatosModelos cadastrados no Projeto.
	 * 
	 * @return List<Artefato>
	 */
	public List<Artefato> listarArtefatosModeloPorProjeto(Projeto projeto) {
		List<Artefato> lista = new ArrayList<Artefato>();
		for (ItemModelo itemModelo : projeto.getModelo().getItemModelo()) {
			Retorno<String, Collection<Artefato>> retorno = getFramework()
					.pesquisarArtefato(itemModelo.getArtefato());
			if (retorno.isSucesso()) {
				lista.addAll(retorno.getParametros().get(
						Retorno.PARAMERTO_LISTA));
			}
		}
		return lista;
	}

	public List<ArtefatoPreenchido> listarArtefatosPreenchidosProjeto(
			Projeto projeto) {
		ArtefatoPreenchido artefatoPreenchido = new ArtefatoPreenchido();
		artefatoPreenchido.setModelo(projeto.getModelo().getCodigo());
		Retorno<String, Collection<ArtefatoPreenchido>> retorno = getFramework()
				.pesquisarArtefatosPreenchidos(artefatoPreenchido);
		if (retorno.isSucesso()) {
			return (List<ArtefatoPreenchido>) retorno.getParametros().get(
					Retorno.PARAMERTO_LISTA);
		}
		return new ArrayList<ArtefatoPreenchido>();
	}

	public ServicoControle getServicoControle() {
		return servicoControle;
	}

	public void setServicoControle(ServicoControle servicoControle) {
		this.servicoControle = servicoControle;
	}

	/**
	 * Retorna o ArtefatoServico que foi foi selecionado na visão para ser
	 * mapeado através do mapa de atributos
	 * 
	 * @return ArtefatoServico artefatoServicoAMapear(
	 */
	private ArtefatoServico getArtefatoServicoAMapear() {
		return (ArtefatoServico) getMapaAtributos().get(
				"artefatoServicoAMapear");
	}

	/**
	 * Retorna o ArtefatoServico contendo o Servico de persistência do
	 * ArtefatoModelo e com os parâmetros configurados a este serviço
	 * 
	 * @return ArtefatoServico servicoPersistencia(
	 */
	private ArtefatoServico getServicoPersistencia() {
		return (ArtefatoServico) getMapaAtributos().get("servicoPersistencia");
	}

}
