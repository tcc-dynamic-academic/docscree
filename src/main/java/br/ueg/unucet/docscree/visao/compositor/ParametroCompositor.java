package br.ueg.unucet.docscree.visao.compositor;

import java.util.Collection;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Window;

import br.ueg.unucet.docscree.controladores.ProjetoControle;
import br.ueg.unucet.quid.extensao.interfaces.IParametro;
import br.ueg.unucet.quid.extensao.interfaces.IServico;
import br.ueg.unucet.quidgerenciadorservico.QuidGerenciadorServico;

/**
 * Compositor que representa a página de montar Artefato Modelo
 * 
 * @author Diego
 *
 */
@SuppressWarnings({"rawtypes", "unchecked"})
@Component
@Scope("session")
public class ParametroCompositor extends GenericoCompositor<ProjetoControle>
{
	public void carregarParametro() {
		Window paleta = (Window) this.getComponent().getFellow("windowParamAtt");
		IServico servico = QuidGerenciadorServico.getInstancia().getServicoControle().getInstanciaServico("persistenciabdjdbc", 1);
		Collection<IParametro<?>> listaParametros = servico.getListaParametros();
		Grid grid = new Grid();
		Rows rows = new Rows();		
		gerarParametrosTipoMembro(rows,servico,"300");
		grid.appendChild(rows);
		paleta.appendChild(grid);
		
		/*for (IParametro<?> parametro : listaParametros) {
			getInstanciaComponente(parametro).setValor(getComponentePorId("PARAMETRO"+parametro.getNome(), paleta), parametro.getValor());
		}*/
		//como é feito no tipoMembro pra montar a lista de parametro?
	}
	
	
	
	

	@Override
	public Class getTipoEntidade() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void limparCampos() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void limparFiltros() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void acaoFiltrar() {
		// TODO Auto-generated method stub
		
	}
	

	
}
